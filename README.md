# Experiments

This is a grab-bag of many projects where I play with a new tool or technology so I can have a personal FAQ if I need to use it.

## Finished

### simple_calculator

The initial idea was using two text boxes and a span to demonstrate Vue binding, but it's growing to include some other
basic concepts too.

## Works in Progress

### name_list

This is going to be an app that implements an editable list of names. That requires a lot of concepts that I'm
not so comfy with yet.